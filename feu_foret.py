from tkinter import *
import random
import sys
import time
import threading

# creation de l'automate

def EstParsable(value):

    try:
        float(value)
        return True
    except ValueError:
        return False

def ParseParametre(argv):
    
    resultat = list()

    #On initialise une liste avec des valeurs prédéfinites
    resultat.append('') 
    resultat.append('') 
    resultat.append('') 
    resultat.append('') 
    resultat.append('')
    
    if len(argv) > 5 :
        sys.exit(1)

    #On met à jour la liste si les paramètres sont valide
    for parametre in argv:
        if "rows=" in parametre:
            if EstParsable(parametre.split("=")[1]):
                resultat[0] = int(parametre.split("=")[1])
        if "cols=" in parametre:
            if EstParsable(parametre.split("=")[1]):
                resultat[1] = int(parametre.split("=")[1])
        if "cell_size=" in parametre:
            if EstParsable(parametre.split("=")[1]):
                resultat[2] = int(parametre.split("=")[1])
        if "afforestation=" in parametre:
            if EstParsable(parametre.split("=")[1]):
                resultat[3] = float(parametre.split("=")[1])
        if "anim_time=" in parametre:
            if EstParsable(parametre.split("=")[1]):
                resultat[4] = float(parametre.split("=")[1])
                
    #On assigne des valeurs par défaut si elle n'ont pas été modifiées
    for index in range(0, 5):
        if resultat[index] == '':
            if index == 0 or index == 1:
                resultat[index] = 10
            if index == 2:
                resultat[index] = 20
            if index == 3:
                resultat[index] = .6
            if index == 4:
                resultat[index] = 2
    
    if resultat[0] * resultat[2] >= 1200 or resultat[1] * resultat[2] >= 900:
        print("Paramètres des carré trop grand pour pouvoir être affiché")
        sys.exit(2)

    return resultat




def InitialisationTabSimulation(parametre):

    new_list = list()
    for ligne in range(0, parametre[0]):
        autreListe=list()
        for colonne in range(0, parametre[1]):
            boisement = random.randint(1, 10)
            if boisement <= parametre[3] * 10: 
                etat = "arbre"
            else:
                etat = "vide"
            autreListe.append(etat)
        new_list.append(autreListe)

    return new_list

def InitialisationCanvas(canvas, parametre, tableauForet):

    #On ajoute les cases 1 par 1
    xPos = 0
    for index_x in tableauForet:
        yPos = 0
        for etat in index_x:
            if etat == "vide":
                canvas.create_rectangle(xPos, yPos, xPos + parametre[2], yPos + parametre[2], fill="black")
            if etat == "arbre":
                canvas.create_rectangle(xPos, yPos, xPos + parametre[2], yPos + parametre[2], fill="green")
            yPos = yPos + parametre[2]
        xPos = xPos + parametre[2]

# avant le debut de l'animation

def IndexCase(x, y, parametre):

    resultat = list()

    if x%parametre != 0 and y%parametre !=0:
        resultat.append(int(x/parametre))
        resultat.append(int(y/parametre))
    else:
        resultat.append(-1)
        resultat.append(-1)

    return resultat


def PlacerFoyer(event, parametre, tableauForet):

    indexCarre = IndexCase(event.x, event.y, parametre[2])
    if tableauForet[indexCarre[0]][indexCarre[1]] == "arbre" :
        tableauForet[indexCarre[0]][indexCarre[1]] = "feu"
        canvas.create_rectangle(indexCarre[0]*parametre[2], indexCarre[1]*parametre[2], indexCarre[0]*parametre[2] + parametre[2], indexCarre[1]*parametre[2] + parametre[2], fill="red")
    elif tableauForet[indexCarre[0]][indexCarre[1]] == "feu" :
        tableauForet[indexCarre[0]][indexCarre[1]] = "arbre"
        canvas.create_rectangle(indexCarre[0]*parametre[2], indexCarre[1]*parametre[2], indexCarre[0]*parametre[2] + parametre[2], indexCarre[1]*parametre[2] + parametre[2], fill="green")
        
# animation

def CopieCarte(tableauForet):

    tmp = list()
    for lists in tableauForet:
        newList = list(lists)
        tmp.append(newList)
    return tmp

def MajCendre(tableauForet):

    tmp = CopieCarte(tableauForet)
    
    for indexX in range(0, len(tmp)) :
        for indexY in range(0, len(tmp[1])) :
            if tmp[indexX][indexY] == "cendre" :
                tmp[indexX][indexY] = "vide"
    
    return tmp

def MajFeu(tableauForet):

    tmp = CopieCarte(tableauForet)
    
    for indexX in range(0, len(tmp)) :
        for indexY in range(0, len(tmp[1])) :
            if tmp[indexX][indexY] == "feu" :
                tmp[indexX][indexY] = "cendre"

    return tmp

def MajArbre(tableauForet):

    tmp = CopieCarte(tableauForet)

    for indexX in range(0, len(tmp)) :
        for indexY in range(0, len(tmp[1])) :
            if tableauForet[indexX][indexY] == "cendre" :
                if indexX - 1 >= 0 :
                    if tableauForet[indexX - 1][indexY] == "arbre" :
                        tmp[indexX - 1][indexY] = "feu"
                if indexX + 1 < len(tmp) :
                    if tableauForet[indexX + 1][indexY] == "arbre" : 
                        tmp[indexX + 1][indexY] = "feu"
                if indexY - 1 >= 0 :
                    if tableauForet[indexX][indexY - 1] == "arbre" :
                        tmp[indexX][indexY - 1] = "feu"
                if indexY + 1 < len(tmp[1]) :
                    if tableauForet[indexX][indexY + 1] == "arbre" :
                        tmp[indexX][indexY + 1] = "feu"
    
    return tmp



def FeuPresent(tableauForet):

    for indexX in tableauForet:
        for etat in indexX:
            if etat == "feu":
                return True
    
    return False

def CendrePresente(tableauForet):
 
    for indexX in tableauForet:
        for etat in indexX:
            if etat == "cendre":
                return True
    
    return False


def MajCanvas(canvas, tableauForet, NouveauTabForet, parametre):

    for indexX in range(len(tableauForet)):
        for indexY in range(len(tableauForet[0])):
            if tableauForet[indexX][indexY] != NouveauTabForet[indexX][indexY]:
                if NouveauTabForet[indexX][indexY] == "feu":
                    canvas.create_rectangle(indexX * parametre[2], indexY * parametre[2], indexX * parametre[2] + parametre[2], indexY * parametre[2] + parametre[2], fill="red")
                if NouveauTabForet[indexX][indexY] == "vide":
                    canvas.create_rectangle(indexX * parametre[2], indexY * parametre[2], indexX * parametre[2] + parametre[2], indexY * parametre[2] + parametre[2], fill="black")
                if NouveauTabForet[indexX][indexY] == "cendre":
                    canvas.create_rectangle(indexX * parametre[2], indexY * parametre[2], indexX * parametre[2] + parametre[2], indexY * parametre[2] + parametre[2], fill="grey")
                if NouveauTabForet[indexX][indexY] == "arbre":    
                    canvas.create_rectangle(indexX * parametre[2], indexY * parametre[2], indexX * parametre[2] + parametre[2], indexY * parametre[2] + parametre[2], fill="green")

def LancerSimulation(tableauForet, parametre, canvas, pause):

    while FeuPresent(tableauForet) or CendrePresente(tableauForet):
        if pause[0] :
            continue
        else:
            tmp = MajCendre(tableauForet)
            tmp = MajFeu(tmp)
            tmp = MajArbre(tmp)
            MajCanvas(canvas, tableauForet, tmp, parametre)
            ##On recopie sans changer la référence
            for index_x in range(len(tmp)):
                for index_y in range(len(tmp[1])):
                    tableauForet[index_x][index_y] = tmp[index_x][index_y]
            time.sleep(parametre[4])



if __name__ == '__main__' :
    
    parametre=ParseParametre(sys.argv)
    tableauForet=InitialisationTabSimulation(parametre)

    master = Tk()

    master.rowconfigure(1, weight=1)
    master.columnconfigure(0, weight=1)
    master.columnconfigure(1, weight=1)
    master.columnconfigure(2, weight=1)
    master.columnconfigure(3, weight=1)

    canvas = Canvas(master, 
                    width=parametre[0]*parametre[2], 
                    height=parametre[1]*parametre[2])
    canvas.grid(row=0, columnspan=4, padx=10, pady=10, sticky='nsew')
    InitialisationCanvas(canvas, parametre, tableauForet)

    def click_callback_foyer(event, parametres=parametre, tableau=tableauForet):
        PlacerFoyer(event, parametres, tableau)

    canvas.bind('<Button-1>', click_callback_foyer)
    canvas.bind('<Button-3>', click_callback_foyer)

    pause = []
    pause.append(True)
    thread = threading.Thread(target=LancerSimulation, args=(tableauForet,parametre,canvas,pause))

    def click_callback_demarer():
        pause[0] = False
        thread.start()

    btn_start_simu = Button(master,
                             text='demarer la simulation',
                             command=click_callback_demarer,
                             bg="pale goldenrod",
                             activebackground="#3498db")
    btn_start_simu.grid(row=1, column=1)


    master.resizable(width=False, height=False)
    master.mainloop()
