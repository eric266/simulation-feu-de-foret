from tkinter import *
import datetime
master=Tk()

master=Tk()

canvas=Canvas(master, width=700, height=250)
canvas.pack()

element_id = canvas.create_oval(10,20,30,40,fill='blue')

def anim():
    if datetime.datetime.now().second%2==0:
        canvas.itemconfig(element_id, fill='yellow')
    else:
        canvas.itemconfig(element_id, fill='blue')
    master.after(500, anim)

anim()
master.mainloop()