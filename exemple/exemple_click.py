from tkinter import *
master=Tk()

master=Tk()

canvas=Canvas(master, width=700, height=250)
canvas.pack()

def click_callback(event):
    x1=event.x
    y1=event.y
    color='blue'
    if event.num==1:
        color='red'
    elif event.num==3:
        color='yellow'
    canvas.create_rectangle(x1,y1,x1+20,y1+20,fill=color)

canvas.bind('<Button-1>',click_callback)
canvas.bind('<Button-3>',click_callback)

master.mainloop()