from tkinter import *

def on_click(event, c):

    """
    allows the user to choose the position of the fire starts

    Parameters
    ----------
    event : event
    c : canvas

    Returns
    -------
    none
    """
    current = event.widget.find_withtag("current")
    if current:
        item = current[0]
        color = c.itemcget(item, "fill")
        if color == "green":
            c.itemconfig(item, fill="red")