# Mini-Projet 4 : simulations de feux de forêt

Projet réalisé en 2ème année de DUT Informatique d'Amiens

Auteurs :
*  Eric MARTIN
*  Arthur PALMA

Pour lancer le programme utiliser la commande py main.py ou py main.py  10, 10, 50, 1, 1 (les arguments représentent, dans l'ordre, le nombre des lignes,
le nombre des colonnes, la taille des cellules, le taux de boisement et la durée de l'animation, vous pouvez les modifiez à votre guise)