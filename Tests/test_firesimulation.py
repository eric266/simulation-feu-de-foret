import unittest
import os
from Argument import Args
from Interface import *
from Frame import *

class FireSimulationTestCase(unittest.TestCase):


    arguments=Args()
    grid = [[0 for x in range(10)] for z in range(10)]
    listerouge = []
    listenoire = []


    def tearDown(self):
        """
        Clear the lists used in the tests
        """
        FireSimulationTestCase.listenoire.clear
        FireSimulationTestCase.listerouge.clear


    def test_arguments(self):
        self.assertIsNotNone(FireSimulationTestCase.arguments)
        self.assertIsInstance(FireSimulationTestCase.arguments.nbligne, int)
        self.assertIsInstance(FireSimulationTestCase.arguments.nbcolonne, int)
        self.assertIsInstance(FireSimulationTestCase.arguments.proba, float)
        self.assertIsInstance(FireSimulationTestCase.arguments.taille, int)
        self.assertIsInstance(FireSimulationTestCase.arguments.duree, int)

    def test_gen_forest(self):
        frame = Frame_init()
        canvas = Canvas_init(frame)
        gen_forest(FireSimulationTestCase.grid, 10, 10, 0.0, 500, 230, 50, canvas)
        self.assertIsNotNone(FireSimulationTestCase.grid)
        self.assertEqual(len(FireSimulationTestCase.grid), 10)
        color = canvas.itemcget(FireSimulationTestCase.grid[0][0], "fill")
        self.assertEqual(color, "white")
        gen_forest(FireSimulationTestCase.grid, 10, 10, 1.0, 500, 230, 50, canvas)
        color = canvas.itemcget(FireSimulationTestCase.grid[0][0], "fill")
        self.assertEqual(color, "green")

    def test_Detect(self):
        frame = Frame_init()
        canvas = Canvas_init(frame)
        gen_forest(FireSimulationTestCase.grid, 10, 10, 0.0, 500, 230, 50, canvas)
        NumberOfFire = Detect(10,10,canvas,FireSimulationTestCase.grid)
        self.assertEqual(NumberOfFire, 0)
        canvas.itemconfig(FireSimulationTestCase.grid[0][0], fill="red")
        NumberOfFire = Detect(10,10,canvas,FireSimulationTestCase.grid)
        self.assertEqual(NumberOfFire, 1)

    def test_Neighborhood_x(self):
        frame = Frame_init()
        canvas = Canvas_init(frame)
        gen_forest(FireSimulationTestCase.grid, 10, 10, 1.0, 500, 230, 50, canvas)
        canvas.itemconfig(FireSimulationTestCase.grid[0][0], fill="red")
        Check_Fire_Lines(FireSimulationTestCase.grid,10,1,0,FireSimulationTestCase.listenoire,FireSimulationTestCase.listerouge,canvas)
        self.assertIsNotNone(FireSimulationTestCase.listenoire)
        color = canvas.itemcget(FireSimulationTestCase.listenoire[0], "fill")
        self.assertEqual(color, "red")
        self.assertIsNotNone(FireSimulationTestCase.listerouge)
        color = canvas.itemcget(FireSimulationTestCase.listerouge[0], "fill")
        self.assertEqual(color, "green")

    def test_Neighborhood_y(self):
        frame = Frame_init()
        canvas = Canvas_init(frame)
        gen_forest(FireSimulationTestCase.grid, 10, 10, 1.0, 500, 230, 50, canvas)
        canvas.itemconfig(FireSimulationTestCase.grid[0][0], fill="red")
        Check_Fire_Columns(FireSimulationTestCase.grid,10,0,1,FireSimulationTestCase.listenoire,FireSimulationTestCase.listerouge,canvas)
        self.assertIsNotNone(FireSimulationTestCase.listenoire)
        color = canvas.itemcget(FireSimulationTestCase.listenoire[0], "fill")
        self.assertEqual(color, "red")
        self.assertIsNotNone(FireSimulationTestCase.listerouge)
        color = canvas.itemcget(FireSimulationTestCase.listerouge[0], "fill")
        self.assertEqual(color, "green")

    def Number_Neighbour_In_Fire(self):
        listenoire = [5, 10, 15, 20]
        self.assertIsNotNone(Number_Neighbour_In_Fire(listenoire))
        test = Number_Neighbour_In_Fire(listenoire)
        self.assertEqual(test, 0.8)

