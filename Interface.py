from tkinter import *
from Frame import Frame_init, gen_forest, Canvas_init
import datetime
import random
import argparse
from Argument import Args
from Select import on_click


def Detect(nbligne,nbcolonne,canvas,grid):

    """
    Detect if one of the rectangle of the grid is red

    Parameters
    ----------
    canvas : canvas
    grid : 2D array of widgets(rectangle)
    nbligne : integer, the number of line of the grid
    nbcolonne : integer, the number of columns of the grid

    Returns
    -------
    none
    """
    detection=0
    for row in range(nbligne):
        for col in range(nbcolonne):
            color = canvas.itemcget(grid[row][col], "fill")
            if color == "red":
                detection=detection + 1
    return detection


def Number_Neighbour_In_Fire(listenoire):
    """
    Used to calculate the probability for a cell to be set on fire with the second method

    Parameters
    ----------
    none

    Returns
    -------
    f : frame
    """
    k = len(listenoire)
    fire_luck = 1 - (1/(k+1))
    return fire_luck


def Check_Fire_Lines(grid, nbligne, x, y, listecendre, listefeu, canvas):
    
    """
    Check if one of the neighbor of the rectangle is in fire (only for lines)

    Parameters
    ----------
    canvas : canvas
    grid : 2D array of widgets(rectangle)
    x : integer, the index of the line of the widget
    y : integer, the index of the line of the widget
    nbligne : integer, the number of columns in the grid
    Listecendre : list of widgets, contains the rectangle that will become cinder
    Listefeu : list of widgets, contains the rectangle that will become fire

    Returns
    -------
    none
    """
    check = canvas.itemcget(grid[x][y], "fill")
    if x-1 > -1 :
        color = canvas.itemcget(grid[x-1][y], "fill")
    if x-1 > -1 and color == "red":
        listecendre.append(grid[x-1][y])
        if check == "green":
            listefeu.append(grid[x][y])
    if x+1 < nbligne :
        color = canvas.itemcget(grid[x+1][y], "fill")
    if x+1 < nbligne and color == "red":
        listecendre.append(grid[x+1][y])
        if check == "green":
            listefeu.append(grid[x][y])


def Check_Fire_Columns(grid,nbcolonne, x, y, listecendre, listefeu, canvas):

    """
    Check if one of the neighbor of the rectangle is in fire (only for columns)

    Parameters
    ----------
    canvas : canvas
    grid : 2D array of widgets(rectangle)
    x : integer, the index of the line of the widget
    y : integer, the index of the line of the widget
    nbcolonne : integer, the number of columns in the grid
    Listecendre : list of widgets, contains the rectangle that will become cinder
    Listefeu : list of widgets, contains the rectangle that will become fire

    Returns
    -------
    none
    """
    check = canvas.itemcget(grid[x][y], "fill")
    if y-1 > -1 :    
        color = canvas.itemcget(grid[x][y-1], "fill")
    if y-1 > -1 and color == "red":
        listecendre.append(grid[x][y-1])
        if check == "green":
            listefeu.append(grid[x][y])
    if y+1 < nbcolonne :
        color = canvas.itemcget(grid[x][y+1], "fill")
    if y+1 < nbcolonne and color == "red":
        listecendre.append(grid[x][y+1])
        if check == "green":
            listefeu.append(grid[x][y])


def Next_Frame(listenoire, listerouge,listeblanche, canvas):
        
    """
    The function used to do the animation

    Parameters
    ----------
    canvas : canvas
    listenoire : list, contains the rectangles which will turn black at t + 1
    listerouge : list, contains the rectangles which will turn red at t + 1
    listeblanche : list, contains the rectangles which will turn white at t + 1

    Returns
    -------
    none
    """

    for item in listeblanche:
        canvas.itemconfig(item, fill="white")
    listeblanche.clear
    for item in listerouge:
        canvas.itemconfig(item, fill="red")
    listerouge.clear
    for item in listenoire:
        canvas.itemconfig(item, fill="black")
        listeblanche.append(item)
    listenoire.clear


def Animation(canvas, grid, args, f, nbligne, nbcolonne, listblanche, methode):

    """
    The function used to do the animation

    Parameters
    ----------
    canvas : canvas
    grid : 2D array of widgets(rectangle)
    args : List of arguments
    f : frame
    nbligne : integer, the number of line of the grid
    nbcolonne : integer, the number of columns of the grid
    listblanche : list

    Returns
    -------
    none
    """

    listrouge=[]
    listnoire=[]
    for row in range(nbligne):
        for col in range(nbcolonne):
            Check_Fire_Lines(grid, nbligne, row, col, listnoire, listrouge, canvas)
            Check_Fire_Columns(grid, nbcolonne, row, col, listnoire, listrouge, canvas)
            if methode == True:
                rand = random.random()
                if rand > Number_Neighbour_In_Fire(listnoire):
                    if listrouge:
                        listrouge.pop()
    Next_Frame(listnoire, listrouge,listblanche, canvas)
    canvas.after(args.duree*1000,Animation,canvas, grid, args, f, nbligne, nbcolonne, listblanche, methode)
        

def Regle1(canvas, grid, args, f, nbligne, nbcolonne, listblanche, methode):

    """
    The simulation will start using the first rule if the user push this button

    Parameters
    ----------
    canvas : canvas
    grid : 2D array of widgets(rectangle)
    args : List of arguments
    f : frame
    nbligne : integer, the number of line of the grid
    nbcolonne : integer, the number of columns of the grid
    listblanche : list

    Returns
    -------
    none
    """

    Animation(canvas, grid, args, f, nbligne, nbcolonne, listblanche, methode)


def Simulation():

    """
    The function than run the simulation by using the others functions

    Parameters
    ----------
    none

    Returns
    -------
    none
    """

    args = Args()
    listblanche=[]
    f = Frame_init()
    c = Canvas_init(f)

    titreLbl = Label(c, text="Simulation de feux de forêt",bg="#A0522D", fg="orange", width="43", font="-size 45")
    titreLbl.grid(column = 0, row = 0, padx="20", columnspan=2)

    n1 = 530
    n2 = 200
    nbligne=args.nbligne
    nbcolonne=args.nbcolonne
    grid = [[0 for x in range(nbligne)] for z in range(nbcolonne)]
    proba=args.proba
    taille=args.taille
    gen_forest(grid, nbligne, nbcolonne, proba, n1, n2, taille, c)

    boutonRegle1 = Button(c, text='Règle 1', width="20", font="-size 13", command= lambda: Regle1(c, grid, args, f, nbligne, nbcolonne, listblanche, False))
    boutonRegle1.grid(column = 0,row = 2, pady="650")

    boutonRegle2 = Button(c, text='Règle 2', width="20", font="-size 13", command= lambda: Regle1(c, grid, args, f, nbligne, nbcolonne, listblanche, True))
    boutonRegle2.grid(column = 1,row = 2, pady="650")

    f.mainloop()