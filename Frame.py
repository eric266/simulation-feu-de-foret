from tkinter import *
from Select import on_click
import random

def Frame_init():
    """
    Used to create the frame

    Parameters
    ----------
    none

    Returns
    -------
    frame : frame
    """

    frame = Tk()
    frame = Frame(frame)
    frame.place(x=0, y=0)
    frame.pack(fill="both", expand=True)
    return frame

def Canvas_init(frame):
    """
    Used to create the canvas

    Parameters
    ----------
    frame : frame
    
    Returns
    -------
    canvas : canvas
    """

    canvas = Canvas(frame,bg="#A0522D")
    canvas.place(x=1, y=1)
    canvas.pack(fill="both", expand=True)
    canvas.bind('<ButtonPress-3>', lambda event, canvas =canvas : on_click(event,canvas))

    return canvas

def gen_forest(grid, nbligne, nbcolonne, proba, n1, n2, taille, canvas):
    """
    Used to generate the forest

    Parameters
    ----------
    grid : 2d array of widgets
    nbligne : integer
    nbcolonne : integer
    proba :float
    n1 : integer
    n2 : integer
    taille : integer
    canvas :canvas
    
    Returns
    -------
    none
    """

    for row in range(nbligne):
        for col in range(nbcolonne):
            rand = random.random()
            if rand > proba:
                myrectangle = canvas.create_rectangle((n1, n2, (taille+n1), (taille+n2)),
                                    outline="black", fill="white", width=2)
                grid[row][col]=myrectangle
            else:
                myrectangle = canvas.create_rectangle((n1, n2, (taille+n1), (taille+n2)),
                                    outline="black", fill="green", width=2)
                grid[row][col]=myrectangle
            n1 = n1+taille
        n1 = 530
        n2 = n2+taille