import argparse

def Args():
    """
    allows the user to choose the parameters used for the simulation

    Parameters
    ----------
    none

    Returns
    -------
    none
    """
    parser = argparse.ArgumentParser(
        description='Fait une simulation de feux de foret avec des automates 2D')
    # Permet d'entrer le fichier à traduire et le fichier recevant la traduction en paramètres
    parser.add_argument('nbligne', help='nombre de lignes', nargs='?', type=int, default=10)
    parser.add_argument('nbcolonne', help='nombre de colonnes', nargs='?', type=int, default=10)
    parser.add_argument('taille', help='taille de cellules', nargs='?', type=int, default=50)
    parser.add_argument('proba', help='probabilité que la cellule soit boisée', nargs='?', type=float, default=0.6)
    parser.add_argument('duree', help='durée des animations en seconde', nargs='?', type=int,  default=1)

    return parser.parse_args()